// Copyright Epic Games, Inc. All Rights Reserved.

#include "HowToWalkYourDragonGameMode.h"
#include "HowToWalkYourDragonCharacter.h"
#include "UObject/ConstructorHelpers.h"

AHowToWalkYourDragonGameMode::AHowToWalkYourDragonGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
