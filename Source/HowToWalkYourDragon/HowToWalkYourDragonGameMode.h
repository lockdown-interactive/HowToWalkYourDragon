// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HowToWalkYourDragonGameMode.generated.h"

UCLASS(minimalapi)
class AHowToWalkYourDragonGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AHowToWalkYourDragonGameMode();
};



