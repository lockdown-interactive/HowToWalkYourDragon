@echo off

setlocal
echo Setting up Unreal Engine 4 project files...
echo Deleting files

@RD /S /Q ".vs"
@RD /S /Q "Binaries"
@RD /S /Q "Saved"
@RD /S /Q "Intermediate"
@RD /S /Q "DerivedDataCache"
DEL /S /Q "HowToWalkYourDragon.sln"

echo Files deleted correctly!
echo Generating Visual Studio project files...

del /q gen_temp.txt
powershell -command "& { (Get-ItemProperty 'Registry::HKEY_CLASSES_ROOT\Unreal.ProjectFile\shell\rungenproj' -Name 'Icon' ).'Icon' }" > gen_temp.txt
set /p gen_bin=<gen_temp.txt
%gen_bin% /projectfiles %cd%\HowToWalkYourDragon.uproject
del /q gen_temp.txt

echo Visual Studio files generated correctly

PAUSE
