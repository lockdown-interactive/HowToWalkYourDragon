![Semantic description of image](https://img.itch.zone/aW1nLzQ1MTE4MzEucG5n/original/5AxbF%2B.png)

HowToWalkYourDragon (Latest branch: Develop)

Videogame developed in one week for the IndieDevDay GameJam.

Download the game for free: https://lockdowninteractive.itch.io/how-to-walk-your-dragon 

## Description:

You are Timmy, a young and stupid boy who thougth that having a dragon as a pet  was a good idea. Like all the pets in the world, the dragon needs to pee, and Timmy must go for a walk with him, but the dragon is so naughty that will do anything he wants.

Try to guide the dragon to the pee points and use Timmy's abilities to prevent the dragon from eating the innocent civilians. How many days can you last until Timmy is  kicked out of town for having a dragon? Compete against your friends and the whole world in the global leaderboard and last more than anyone!

A game created for the IndieDevDay Game Jam 2020 with the theme "I don't think it's a good idea".
